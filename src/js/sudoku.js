function sudoku(variable, options) {
    if (options["solver"] == "FC")
        return sudokuFC(variable, options["heuristic"]);
    return sudokuBT(variable);
}

function checkConstrain(variable, x, y, n) {
    for (var i = 0; i < variable.length; i++)
        if ((variable[x][i] == variable[x][y] && i != y) || (variable[i][y] == variable[x][y] && i != x))
            return false;
    var xStart = x,
        yStart = y;
    while(xStart % n != 0)
        xStart--;
    while(yStart % n != 0)
        yStart--;
    for (var i = xStart; i < xStart + n; i++)
        for (var j = yStart; j < yStart + n; j++)
            if (variable[i][j] == variable[x][y] && i != x && j != y)
                return false;

    return true;
}

function sudokuBT(variable) {
    console.log("Init sudoku BT...");
    var n = variable.length;
    var sqrt = Math.sqrt(n);
    var constvar = new Array(n);
    for (var i = 0; i < n; i++) {
        constvar[i] = new Array(n);
        for (var j = 0; j < n; j++)
            if (variable[i][j])
                constvar[i][j] = true;
            else
                variable[i][j] = 0;
    }
    var domain = n;
    var iteration = 0;

    console.log("Sudoku BT started...");
    var start = performance.now();
    var x = 0,
        y = 0;
    var goToRight = true;
    while (y < n && y > -1) {
        if (constvar[x][y]) {
            if (goToRight) {
                x++;
                if (x >= n) {
                    x = 0;
                    y++;
                }
            }
            else {
                x--;
                if (x < 0) {
                    x = n - 1;
                    y--;
                }
            }
        }
        else {
            iteration++;
            variable[x][y]++;
            if (variable[x][y] <= domain) {
                if (checkConstrain(variable, x, y, sqrt)) {
                    x++;
                    if (x >= n) {
                        x = 0;
                        y++;
                    }
                    goToRight = true;
                }
            }
            else {
                variable[x][y] = 0;
                x--;
                if (x < 0) {
                    x = n - 1;
                    y--;
                }
                goToRight = false;
            }
        }
    }
    var end = performance.now();
    console.log("Time: " + (end - start));
    console.log("Iterations: " + iteration);

    if (y == -1) {
        console.log("Result not found.");
        return undefined;
    }
    var result = { result: variable, time: (end - start), iterations: iteration, solver: "BT" };
    return result;
}

function sudokuFC(variable, heuristic) {
    console.log("Init sudoku FC...");
    var n = variable.length;
    var sqrt = Math.sqrt(n);
    var constvar = new Array(n);
    var old_domain = new Array(n);
    var domain = new Array(n);
    var nextVariables = new Array();
    var lastVariables = new Array();
    var counter = new Array(n + 1);
    for (var i = 0; i < n; i++) {
        counter[i + 1] = 0;
        constvar[i] = new Array(n);
        old_domain[i] = new Array(n);
        domain[i] = new Array(n);
        for (var j = 0; j < n; j++) {
            old_domain[i][j] = new Array(n);
            if (variable[i][j]) {
                constvar[i][j] = true;
            }
            else
                variable[i][j] = 0;
            if (!constvar[i][j])
                domain[i][j] = new Array(n);
            for (var k = 0; k < n; k++) {
                if (!constvar[i][j])
                    domain[i][j][k] = k + 1;
                old_domain[i][j][k] = new Array(n);
            }
        }
    }

    for (var x = 0; x < n; x++)
        for(var y = 0; y < n; y++) {
            for (var i = 0; i < n; i++) {
                if (domain[i][y]) {
                    var index = domain[i][y].indexOf(Math.abs(variable[x][y]));
                    if (index > -1)
                        domain[i][y].splice(index, 1);
                }
                if (domain[x][i]) {
                    var index = domain[x][i].indexOf(Math.abs(variable[x][y]));
                    if (index > -1)
                        domain[x][i].splice(index, 1);
                }
            }
            var xStart = x,
                yStart = y;
            while (xStart % sqrt != 0)
                xStart--;
            while (yStart % sqrt != 0)
                yStart--;
            for (var i = xStart; i < xStart + sqrt; i++)
                for (var j = yStart; j < yStart + sqrt; j++) {
                    if (domain[i][j]) {
                        var index = domain[i][j].indexOf(Math.abs(variable[x][y]));
                        if (index > -1)
                            domain[i][j].splice(index, 1);
                    }
                }
        }

    for (var i = n - 1; i > -1; i--)
        for (var j = n - 1; j > -1; j--)
            nextVariables.push({x: j, y: i});
    var iteration = 0;

    function getNext(x, y, next) {
        if (next || !x) {
            if (nextVariables.length < 1)
                return n;
            else {
                var min = nextVariables.length - 1;
                for (var j = nextVariables.length - 1; j > -1; j--) {
                    if (domain[nextVariables[j].x][nextVariables[j].y]) {
                        if (domain[nextVariables[j].x][nextVariables[j].y].length < domain[nextVariables[min].x][nextVariables[min].y].length)
                            min = j;
                    }
                    else
                        return nextVariables.splice(j, 1)[0];
                }
                if (x)
                    lastVariables.push({x: x, y: y});
                return nextVariables.splice(min, 1)[0];
            }
        }
        else {
            if (lastVariables.length < 1)
                return -1;
            else {
                nextVariables.push({x: x, y: y});
                return lastVariables.pop();
            }
        }
    }

    function getNextValue(x, y) {
        if (domain[x][y].length < 1)
            return undefined;
        else {
            var min = domain[x][y][0];
            for (var i = 0; i < domain[x][y].length; i++)
                if (counter[domain[x][y][i]] < counter[min])
                    min = domain[x][y][i];
            var index = domain[x][y].indexOf(Math.abs(min));
            if (index > -1)
                domain[x][y].splice(index, 1);
            counter[min]++;
            return min;
        }
    }

    console.log("Sudoku FC started...");
    var start = performance.now();
    var x = 0,
        y = 0;
    var nextXY;
    if (heuristic) {
        nextXY = getNext();
        x = nextXY.x;
        y = nextXY.y;
    }
    var goToRight = true;
    while (y < n && y > -1) {
        if (constvar[x][y]) {
            if (goToRight) {
                if (heuristic) {
                    nextXY = getNext(x, y, true);
                    x = nextXY.x;
                    y = nextXY.y;
                }
                else {
                    x++;
                    if (x >= n) {
                        x = 0;
                        y++;
                    }
                }
            }
            else {
                if (heuristic) {
                    nextXY = getNext(x, y, false);
                    x = nextXY.x;
                    y = nextXY.y;
                }
                else {
                    x--;
                    if (x < 0) {
                        x = n - 1;
                        y--;
                    }
                }
            }
        }
        else {
            iteration++;
            if (heuristic)
                variable[x][y] = getNextValue(x, y);
            else
                variable[x][y] = domain[x][y].shift();
            if (variable[x][y]) {
                for (var i = 0; i < n; i++)
                    for (var j = 0; j < n; j++) {
                        if (domain[i][j]) {
                            var l = domain[i][j].length;
                            old_domain[x][y][i][j] = new Array(l);
                            while (l--) {
                                old_domain[x][y][i][j][l] = domain[i][j][l];
                            }
                        }
                    }
                for (var i = 0; i < n; i++) {
                    if (domain[i][y]) {
                        var index = domain[i][y].indexOf(Math.abs(variable[x][y]));
                        if (index > -1)
                            domain[i][y].splice(index, 1);
                    }
                    if (domain[x][i]) {
                        var index = domain[x][i].indexOf(Math.abs(variable[x][y]));
                        if (index > -1)
                            domain[x][i].splice(index, 1);
                    }
                }
                var xStart = x,
                    yStart = y;
                while(xStart % sqrt != 0)
                    xStart--;
                while(yStart % sqrt != 0)
                    yStart--;
                for (var i = xStart; i < xStart + sqrt; i++)
                    for (var j = yStart; j < yStart + sqrt; j++) {
                        if (domain[i][j]) {
                            var index = domain[i][j].indexOf(Math.abs(variable[x][y]));
                            if (index > -1)
                                domain[i][j].splice(index, 1);
                        }
                    }
                if (heuristic) {
                    nextXY = getNext(x, y, true);
                    x = nextXY.x;
                    y = nextXY.y;
                }
                else {
                    x++;
                    if (x >= n) {
                        x = 0;
                        y++;
                    }
                }
                goToRight = true;
            }
            else {
                variable[x][y] = 0;
                if (heuristic) {
                    nextXY = getNext(x, y, false);
                    x = nextXY.x;
                    y = nextXY.y;
                }
                else {
                    x--;
                    if (x < 0) {
                        x = n - 1;
                        y--;
                    }
                }
                if (y > -1) {
                    for (var i = 0; i < n; i++)
                        for (var j = 0; j < n; j++) {
                            if (old_domain[x][y][i][j]) {
                                var l = old_domain[x][y][i][j].length;
                                domain[i][j] = new Array(l);
                                while (l--) {
                                    domain[i][j][l] = old_domain[x][y][i][j][l];
                                }
                            }
                        }
                }
                goToRight = false;
            }
        }
    }
    var end = performance.now();
    console.log("Time: " + (end - start));
    console.log("Iterations: " + iteration);

    if (y == -1) {
        console.log("Result not found.");
        return undefined;
    }
    var result = { result: variable, time: (end - start), iterations: iteration, solver: "FC", heuristic: heuristic };
    return result;
}