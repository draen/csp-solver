function hetman(n, options) {
    if (options["solver"] == "FC")
        return hetmanFC(n, options["heuristic"]);
    return hetmanBT(n);
}

function checkConstrain(variables, constrain, n) {
    for (var i = n - 1; i >= 0; i--) {
        var diff = Math.abs(variables[n] - variables[i]);
        if (diff == 0 || diff == constrain[i])
            return false;
    }
    return true;
}

function hetmanBT(n) {
    console.log("Init hetman BT...");
    var variable = new Array(n);
    var domain = n;
    var constrain = new Array(n);
    for (var i = 0; i < n; i++) {
        variable[i] = 0;
        constrain[i] = new Array(i);
        for (var j = 0; j < i; j++) {
            constrain[i][j] = i - j;
        }
    }
    var iteration = 0;

    console.log("Hetman BT started...");
    var start = performance.now();
    var i = 0;
    while(i < n && i > -1) {
        iteration++;
        variable[i]++;
        if (variable[i] <= domain) {
            if (checkConstrain(variable, constrain[i], i)) {
                i++;
            }
        }
        else {
            variable[i] = 0;
            i--;
        }

    }
    var end = performance.now();
    console.log("Time: " + (end - start));
    console.log("Iterations: " + iteration);

    if (i == -1)
        return undefined;
    var result = { result: variable, time: (end - start), iterations: iteration, solver: "BT" };
    return result;
}

function hetmanFC(n, heuristic) {
    console.log("Init hetman FC...");
    var variable = new Array(n);
    var domain = new Array(n);
    var constrain = new Array(n);
    var old_domain = new Array(n);
    var nextVariables = new Array();
    var lastVariables = new Array();
    var counter = new Array(n + 1);
    for (var i = 0; i < n; i++) {
        counter[i + 1] = 0;
        variable[i] = 0;
        domain[i] = new Array(n);
        old_domain[i] = new Array(n);
        for (var j = 0; j < n; j++)
            domain[i][j] = j + 1;
        constrain[i] = new Array(i);
        for (var j = 0; j < i; j++) {
            constrain[i][j] = i - j;
        }
    }
    for (var i = n - 1; i > 0; i--)
        nextVariables.push(i);
    var iteration = 0;

    function getNext(i, next) {
        if (next) {
            if (nextVariables.length < 1)
                return n;
            else {
                var min = nextVariables.length - 1;
                for (var j = nextVariables.length - 1; j > -1; j--)
                    if (domain[nextVariables[j]].length < domain[nextVariables[min]].length)
                        min = j;
                lastVariables.push(i);
                return nextVariables.splice(min, 1)[0];
            }
        }
        else {
            if (lastVariables.length < 1)
                return -1;
            else {
                nextVariables.push(i);
                return lastVariables.pop();
            }
        }
    }

    function getNextValue(i) {
        if (domain[i].length < 1)
            return undefined;
        else {
            var min = domain[i][0];
            for (var j = 0; j < domain[i].length; j++)
                if (counter[domain[i][j]] < counter[min])
                    min = domain[i][j];
            var index = domain[i].indexOf(Math.abs(min));
            if (index > -1)
                domain[i].splice(index, 1);
            counter[min]++;
            return min;
        }
    }

    console.log("Hetman FC started...");
    var start = performance.now();
    var i = 0;
    while(i < n && i > -1) {
        iteration++;
        if (heuristic)
            variable[i] = getNextValue(i);
        else
            variable[i] = domain[i].shift();
        if (variable[i]) {
            for (var j = 0; j < n; j++) {
                var l = domain[j].length;
                old_domain[i][j] = new Array(l);
                while(l--) { old_domain[i][j][l] = domain[j][l]; }
            }
            for (var j = 0; j < n; j++) {
                var index = domain[j].indexOf(variable[i]);
                if (index > -1)
                    domain[j].splice(index, 1);
                index = domain[j].indexOf(variable[i] - Math.abs(j - i));
                if (index > -1)
                    domain[j].splice(index, 1);
                index = domain[j].indexOf(variable[i] + Math.abs(j - i));
                if (index > -1)
                    domain[j].splice(index, 1);
            }
            if (heuristic)
                i = getNext(i, true);
            else
                i++;
        }
        else {
            variable[i] = 0;
            if (heuristic)
                i = getNext(i, false);
            else
                i--;
            if (i > -1)
                for (var j = 0; j < n; j++) {
                    var l = old_domain[i][j].length;
                    domain[j] = new Array(l);
                    while(l--) { domain[j][l] = old_domain[i][j][l]; }
                }
        }
    }
    var end = performance.now();
    console.log("Time: " + (end - start));
    console.log("Iterations: " + iteration);

    if (i == -1)
        return undefined;
    var result = { result: variable, time: (end - start), iterations: iteration, solver: "FC", heuristic: heuristic };
    return result;
}